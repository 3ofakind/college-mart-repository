//
//  SignupViewController.swift
//  College Mart
//
//  Created by Sankati,Vignan on 3/13/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var retypePasswordTF: UITextField!
    @IBOutlet weak var mobilePhoneTF: UITextField!
    @IBOutlet weak var signUp: UIButton!
    
    //To check for every validation an array of boolean values is used
    var validationCheck:[Bool] = [false,false,false,false,false]
    
    var register:KCSAppdataStore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNameTF.delegate = self
        lastNameTF.delegate = self
        passwordTF.delegate = self
        retypePasswordTF.delegate = self
        emailTF.delegate = self
        mobilePhoneTF.delegate = self
        
        // Do any additional setup after loading the view.
        register = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "userProfile",
            KCSStoreKeyCollectionTemplateClass : UserProfile.self
            ])
        
        self.view.addBackground("signup.png")
        firstNameTF.layer.borderColor = UIColor.blackColor().CGColor
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Validation of Firstname textbox
    @IBAction func firstnameTFEditEnd(sender: AnyObject) {
        let firstnameRegEx = "[A-Za-z][A-Z0-9a-z]+"
        let firstnameTest = NSPredicate(format:"SELF MATCHES %@", firstnameRegEx)
        let result = firstnameTest.evaluateWithObject(firstNameTF.text)
        if result == true {
            
            firstNameTF.layer.borderWidth = 0
            validationCheck[0] = true
            totalValidation()
        }
        else {
            firstNameTF.shake()
            validationCheck[0] = false
            firstNameTF.layer.borderWidth = 1
            firstNameTF.layer.borderColor = UIColor.redColor().CGColor
        }
    }
    
    //Validation for Lastname textbox
    @IBAction func lastnameTFEditEnd(sender: AnyObject) {
        let lastnameRegEx = "[A-Za-z][A-Z0-9a-z]+"
        let lastnameTest = NSPredicate(format:"SELF MATCHES %@", lastnameRegEx)
        let result = lastnameTest.evaluateWithObject(lastNameTF.text)
        if result == true {
            validationCheck[1] = true
            lastNameTF.layer.borderWidth = 0
            totalValidation()
        }
        else {
            lastNameTF.shake()
            validationCheck[1] = false
            lastNameTF.layer.borderWidth = 1
            lastNameTF.layer.borderColor = UIColor.redColor().CGColor
        }
    }
    
    //Validation for password textbox
    @IBAction func passwordEditEnd(sender: AnyObject) {
        if passwordTF.text != "" && retypePasswordTF.text == passwordTF.text {
            validationCheck[2] = true
            retypePasswordTF.layer.borderWidth = 0
            passwordTF.layer.borderWidth = 0
            totalValidation()
        }
        else {
            passwordTF.shake()
            retypePasswordTF.shake()
            validationCheck[2] = false
            retypePasswordTF.layer.borderWidth = 1
            retypePasswordTF.layer.borderColor = UIColor.redColor().CGColor
            passwordTF.layer.borderWidth = 1
            passwordTF.layer.borderColor = UIColor.redColor().CGColor
        }
    }
    
    //Validation for phonenumber
    @IBAction func phonenumberEditEnd(sender: AnyObject) {
        let mobileRegEx = "[0-9]{10}"
        let mobileTest = NSPredicate(format:"SELF MATCHES %@", mobileRegEx)
        let result = mobileTest.evaluateWithObject(mobilePhoneTF.text)
        if result == true {
            validationCheck[3] = true
            mobilePhoneTF.layer.borderWidth = 0
            totalValidation()
        }
        else {
            mobilePhoneTF.shake()
            validationCheck[3] = false
            mobilePhoneTF.layer.borderWidth = 1
            mobilePhoneTF.layer.borderColor = UIColor.redColor().CGColor
        }
    }
    
    //Validation for email textbox
    @IBAction func emailTFEditEnd(sender: AnyObject) {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluateWithObject(emailTF.text)
        if result == true {
            validationCheck[4] = true
            emailTF.layer.borderWidth = 0
            totalValidation()
        }
        else {
            emailTF.shake()
            validationCheck[4] = false
            emailTF.layer.borderWidth = 1
            emailTF.layer.borderColor = UIColor.redColor().CGColor
        }
    }
    
    //Check the every validation
    func totalValidation() -> Bool {
        if validationCheck[0] && validationCheck[1] && validationCheck[2] && validationCheck[3] && validationCheck[4] {
            return true
        }
        else {
            return false
        }
    }
    
    //Function to be performed when Signup button is clicked
    func signUp(sender: UIButton) -> Bool {
        var successSignup:Bool = false
        //If all the validations are satisfied then the registration get success
        if totalValidation() {
            let registrationDetails:UserProfile = UserProfile(firstName: firstNameTF.text!, lastName: lastNameTF.text!, email: emailTF.text!,  phone: mobilePhoneTF.text! )
            register.saveObject(registrationDetails, withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil != nil {
                    //save failed
                    successSignup = false
                } else {
                    //save was successful
                    KCSUser.userWithUsername(self.emailTF.text!, password: self.passwordTF.text!, fieldsAndValues: nil, withCompletionBlock: { (user:KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                        if errorOrNil == nil {
                            //Adding User to KCSUser
                            self.displayAlertControllerWithTitle("Success", message: "Registered successfully. Please login to continue")
                        }
                        else{
                            self.displayAlertControllerWithTitle("Failed", message: "Email id already registered")
                        }
                    })
                    successSignup = true
                }
                }, withProgressBlock: nil
            )
        }
        else {
            displayAlertControllerWithTitle("Signup failed", message: "Please Provide all the fields")
        }
        return successSignup
    }
    
    //Segue to be performed
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        return signUp(signUp)
    }
    
    //Function to implement the return button functionality
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let nextTag: NSInteger = textField.tag + 1;
        // Try to find next responder
        if let nextResponder: UIResponder! = textField.superview!.viewWithTag(nextTag){
            nextResponder.becomeFirstResponder()
        }
        else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        return false // We do not want UITextField to insert line-breaks.
    }
    
    override func viewWillAppear(animated: Bool) {
        firstNameTF.text = ""
        lastNameTF.text = ""
        emailTF.text = ""
        passwordTF.text = ""
        retypePasswordTF.text = ""
        mobilePhoneTF.text = ""
    }
    
    //Alertbox
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{(action:UIAlertAction)->Void in  }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
}
