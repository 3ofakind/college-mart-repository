//
//  HomeProductDetailedViewController.swift
//  College Mart
//
//  Created by Aerpula,Venkatesh on 4/13/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import UIKit

class HomeProductDetailedViewController: UIViewController {
    
    var product:ProductAd!
    var myUserProfile:UserProfile!
    var productImage:UIImage!
    @IBOutlet weak var productNameLBL: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productPriceLBL: UILabel!
    @IBOutlet weak var problemDescriptionTV: UITextView!
    var productAdCollection:KCSAppdataStore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        myUserProfile = appDel.userProfiledel
        // Do any additional setup after loading the view.
        problemDescriptionTV.layer.cornerRadius = 5
        problemDescriptionTV.layer.borderColor = UIColor.grayColor().colorWithAlphaComponent(0.5).CGColor
        problemDescriptionTV.layer.borderWidth = 0.5
        problemDescriptionTV.clipsToBounds = true
        productAdCollection = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ProductAdCollection",
            KCSStoreKeyCollectionTemplateClass : ProductAd.self
            ])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeDetailedView(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        productNameLBL.text = product.productName
        productPriceLBL.text = "$ " + String(product.price)
        problemDescriptionTV.text = product.productDescription
        
        
        let productImageName = product.productImage
        
        if productImageName != nil && productImageName != "NULL"
        {
            
            KCSFileStore.downloadFile(
                productImageName,
                options: nil,
                completionBlock: { (downloadedResources: [AnyObject]!, error: NSError!) -> Void in
                    if error == nil {
                        let file = downloadedResources[0] as! KCSFile
                        let fileURL = file.localURL
                        let image = UIImage(contentsOfFile: fileURL.path!)
                        self.productImageView.image = image
                    } else {
                        //Add any error messages here
                    }
                },
                progressBlock: nil
            )
        }
        else
        {
            /* Default Image */
            productImageView.image = UIImage(named: "sell tag icon.png")
        }
    }
    
    //Function to be performed on clicking on Hold Product button
    @IBAction func placeHoldOnProduct(sender: AnyObject) {
        //Confirmation alertbox
        let alertController = UIAlertController(title: "Hold Product", message: "Are you sure you want to hold?", preferredStyle: .Alert)
        //Action to be performed on clicking cancel button in alertbox
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            // ...
        }
        alertController.addAction(cancelAction)
        //Action to be performed on clicking confirm button in alertbox
        let OKAction = UIAlertAction(title: "Confirm", style: .Default) { (action) in
            // ...
            
            //Update condition
            self.product.holdBy = self.myUserProfile.email
            self.product.soldStatus = "Hold"
            self.product.holdStatus = "Hold"
            
            //Update the product to collection
            self.productAdCollection.saveObject(
                self.product,
                withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                    if errorOrNil != nil {
                        //save failed
                        self.displayAlertControllerWithTitle("Error in posting", message: "Invalid input")
                    } else {
                        //save was successful
                        self.dismissViewControllerAnimated(true, completion: nil)
                        
                    }
                },
                withProgressBlock: nil
            )
        }
        alertController.addAction(OKAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    //Alert box
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{(action:UIAlertAction)->Void in  }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    
    
}
