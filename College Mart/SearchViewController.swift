//
//  SearchViewController.swift
//  College Mart
//
//  Created by Sankati,Vignan on 3/10/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
    
    var productAdsFiltered = [ProductAd]()
    var searchController : UISearchController!
    var resultsController = UITableViewController()
    var products:[ProductAd]=[]
    var productAdCollection:KCSAppdataStore!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        // Do any additional setup after loading the view.
        productAdCollection = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ProductAdCollection",
            KCSStoreKeyCollectionTemplateClass : ProductAd.self
            ])
        self.resultsController.tableView.dataSource = self
        self.resultsController.tableView.delegate = self
        //Displaying Search bar in top of TableviewController
        self.searchController = UISearchController(searchResultsController: self.resultsController)
        self.tableView.tableHeaderView = self.searchController.searchBar
        self.searchController.searchResultsUpdater = self
    }
    
    
    /* When ever this view appears */
    override func viewWillAppear(animated: Bool) {
        
        products.removeAll()
        //Initially load the tableView from Kinvey with product Ads soldStatus as Available
        let query = KCSQuery(onField: "soldStatus", withExactMatchForValue: "Available")
        productAdCollection.queryWithQuery(query,withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
            
            for objects in objectsOrNil
            {
                let object = objects as! ProductAd
                self.products.append(ProductAd(productName: object.productName, productDescription: object.productDescription, price: object.price, productImage: object.productImage, holdStatus: object.holdStatus, soldStatus: object.soldStatus, postedBy:  object.postedBy, holdBy:  object.holdBy, entityId: object.entityId!))
            }
            self.tableView.reloadData()
            
            },
            withProgressBlock: nil
        )
        
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        //clousre to get all the filtered results into productAdsFiltered.
        self.productAdsFiltered = self.products.filter { (products:ProductAd) -> Bool in
            if products.productName.lowercaseString.containsString(self.searchController.searchBar.text!.lowercaseString) {
                return true
            } else {
                return false
            }
        }
        
        self.resultsController.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // count number of filtered results if searched for any string or else count number of all results
        if tableView == self.tableView {
            return self.products.count
        } else {
            return self.productAdsFiltered.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        cell = self.tableView.dequeueReusableCellWithIdentifier("search_cell")! as UITableViewCell
        // Display only filtered results if any searched for any string or else display all results
        if tableView == self.tableView {
            cell.textLabel?.text = products[indexPath.row].productName
        } else {
            cell.textLabel?.text = productAdsFiltered[indexPath.row].productName
        }
        
        return cell
    }
    
}
