//
//  ProductAd.swift
//  College Mart
//
//  Created by Putta,Veerapratap Nadh on 3/15/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import Foundation

class ProductAd :NSObject {
    
    var entityId : String?
    var productName:String!
    var productDescription:String!
    var price:NSNumber!
    var productImage:String!
    var holdStatus:String!
    var soldStatus:String!
    var postedBy:String!
    var holdBy:String!
    
    override init() {
        super.init()
    }
    
    init(productName:String!, productDescription:String, price:NSNumber, productImage:String, holdStatus:String, soldStatus:String, postedBy:String, holdBy:String) {
        
        self.productName = productName
        self.productDescription = productDescription
        self.price = price
        self.productImage = productImage
        self.holdStatus = holdStatus
        self.soldStatus = soldStatus
        self.postedBy = postedBy
        self.holdBy = holdBy
    }
    
    convenience init(productName:String!, productDescription:String, price:NSNumber, productImage:String, holdStatus:String, soldStatus:String, postedBy:String, holdBy:String, entityId:String) {
        
        self.init(productName:productName,productDescription:productDescription, price:price, productImage:productImage, holdStatus:holdStatus, soldStatus:soldStatus, postedBy:postedBy, holdBy:holdBy)
        self.entityId = entityId
        
    }
    
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "productName" : "productName",
            "productDescription" : "productDescription",
            "price" : "price",
            "productImage" : "productImage",
            "holdStatus" : "holdStatus",
            "soldStatus" : "soldStatus",
            "postedBy" : "postedBy",
            "holdBy" : "holdBy"
            
        ]
    }
}