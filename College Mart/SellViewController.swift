//
//  SecondViewController.swift
//  College Mart
//
//  Created by Sankati,Vignan on 3/10/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import UIKit

class SellViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, UITextFieldDelegate {
    
    var productAds:ProductAd!
    var myUserProfile:UserProfile!
    
    @IBOutlet weak var productNameTF: UITextField!
    @IBOutlet weak var productPriceTF: UITextField!
    
    @IBOutlet weak var postAdBTN: UIButton!
    @IBOutlet var addImageBTN: UIButton!
    @IBOutlet weak var productIV: UIImageView!
    @IBOutlet weak var descriptionTV: UITextView!
    var validationCheck:[Bool] = [false,false]
    var productAdCollection:KCSAppdataStore!
    
    //Read the image picked from UIImagePickerController
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        self.dismissViewControllerAnimated(true, completion: nil)
        importedImage.image = image
        
    }
    
    //    Importing image
    @IBAction func importImage(sender: UIButton) {
        
        let myActionSheet = UIAlertController(title: "Select image", message: "Where would you add images from?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        // choose from photos action button
        let chooseAction = UIAlertAction(title: "Choose from photos", style: UIAlertActionStyle.Default) { (action) in
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            image.allowsEditing = false
            
            self.presentViewController(image, animated: true, completion: nil)
        }
        
        // take photo action button
        let takePhotoAction = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.Default) { (action) in
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.Camera
            image.allowsEditing = false
            
            self.presentViewController(image, animated: true, completion: nil)
        }
        
        // cancel action button
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) in
        }
        
        // add action buttons to action sheet
        myActionSheet.addAction(chooseAction)
        myActionSheet.addAction(takePhotoAction)
        myActionSheet.addAction(cancelAction)
        
        // support iPads (popover view)
        if let popoverController = myActionSheet.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
        }
        self.presentViewController(myActionSheet, animated: true, completion: nil)
        
    }
    
    @IBOutlet var importedImage: UIImageView!
    
    /* Upload image to kinvey FileStore */
    func uploadImage(image:UIImage) {
        let data = UIImageJPEGRepresentation(image, 0.5) //convert to a 90% quality jpeg
        let metadata = KCSMetadata()
        metadata.setGloballyReadable(true)
        KCSFileStore.uploadData(
            data,
            options: [KCSFileFileName : "\(descriptionTV.text)",
                KCSFileMimeType : "image/jpeg",
                KCSFileId : "\(descriptionTV.text)",
                KCSFileACL : metadata
            ],
            completionBlock: { (uploadInfo: KCSFile!, error: NSError!) -> Void in
            },
            progressBlock: nil
        )
    }
    
    @IBAction func postAdBTN(sender: AnyObject) {
        
        if productNameTF.text != "" && productPriceTF.text != "" && descriptionTV.text != "" {
            let numberFormatter = NSNumberFormatter()
            let price:NSNumber = numberFormatter.numberFromString(productPriceTF.text!)!
            // declaring alertview controller
            let alertController = UIAlertController(title: "Confirm your Ad", message: "Product name: \(self.productNameTF.text!) \nPrice: $\(self.productPriceTF.text!)\nProduct Description: \(descriptionTV.text)", preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                // ...
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "Confirm", style: .Default) { (action) in
                // ...
                self.productAds = ProductAd(productName: self.productNameTF.text, productDescription: self.descriptionTV.text, price: price, productImage: self.descriptionTV.text!, holdStatus: "Created", soldStatus: "Available", postedBy: self.myUserProfile.email, holdBy: "")
                //Posting the product to collection
                self.productAdCollection.saveObject(
                    self.productAds,
                    withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                        if errorOrNil != nil {
                            //save failed
                            NSLog("Save failed, with error: %@", errorOrNil.localizedFailureReason!)
                            self.displayAlertControllerWithTitle("Error in posting", message: "Invalid input")
                        } else {
                            //save was successful
                            /** Uploads image to kinvey here ***/
                            self.uploadImage(self.importedImage.image!)
                            self.displayAlertControllerWithTitle("Posted Successfully", message: "Posted AD: \(self.productNameTF.text!)")
                            self.importedImage.image = nil
                            self.productNameTF.text = nil
                            self.productPriceTF.text = nil
                            self.descriptionTV.text = nil
                        }
                    },
                    withProgressBlock: nil
                )
            }
            alertController.addAction(OKAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            if productNameTF.text == "" {
                displayAlertControllerWithTitle("Posting Ad failed", message: "Please provide product name")
            } else if productPriceTF.text == "" {
                displayAlertControllerWithTitle("Posting Ad failed", message: "Please provide product price")
            } else if descriptionTV.text == "" {
                displayAlertControllerWithTitle("Posting Ad failed", message: "Please provide product description")
            }
            
        }
        
        
        
        
    }
    
    override func viewDidLoad() {
        
        let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        myUserProfile = appDel.userProfiledel
        
        productNameTF.delegate = self
        productPriceTF.delegate = self
        descriptionTV.delegate = self
        
        super.viewDidLoad()
        descriptionTV.delegate = self
        if descriptionTV.text == ""
        {
            textViewDidEndEditing(descriptionTV)
        }
        let tapDimissed = UITapGestureRecognizer(target: self, action: "dismissKeyBoard")
        self.view.addGestureRecognizer(tapDimissed)
        
        //Set border color for textView
        descriptionTV.layer.cornerRadius = 5
        descriptionTV.layer.borderColor = UIColor.grayColor().colorWithAlphaComponent(0.5).CGColor
        descriptionTV.layer.borderWidth = 0.5
        descriptionTV.clipsToBounds = true
        
        //Set Default image
        importedImage.image = UIImage(named: "Upload to Cloud-80.png")
        productAdCollection = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ProductAdCollection",
            KCSStoreKeyCollectionTemplateClass : ProductAd.self
            ])
        
        self.view.addBackground("sell.jpg")
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let nextTag: NSInteger = textField.tag + 1;
        // Try to find next responder
        if let nextResponder: UIResponder! = textField.superview!.viewWithTag(nextTag){
            nextResponder.becomeFirstResponder()
        }
        else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        return false // We do not want UITextField to insert line-breaks.
    }
    
    func dismissKeyBoard()
    {
        descriptionTV.resignFirstResponder()
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == ""
        {
            textView.text = "Enter Description here"
            textView.textColor = UIColor.lightGrayColor()
        }
        textView.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "Enter Description here"
        {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
        textView.becomeFirstResponder()
    }
    
    //    Validations    
    @IBAction func productNameTFValidation(sender: AnyObject) {
        let firstnameRegEx = "[A-Za-z][A-Z0-9a-z ]+"
        let firstnameTest = NSPredicate(format:"SELF MATCHES %@", firstnameRegEx)
        let result = firstnameTest.evaluateWithObject(productNameTF.text)
        if result == true {
            productNameTF.layer.borderWidth = 0
            validationCheck[0] = true
        }
        else {
            validationCheck[0] = false
            productNameTF.layer.borderWidth = 1
            productNameTF.layer.borderColor = UIColor.redColor().CGColor
        }
    }
    
    @IBAction func priceTFValidation(sender: AnyObject) {
        let priceRegEx = "[0-9]+(.[0-9][0-9]?)?"
        let mobileTest = NSPredicate(format:"SELF MATCHES %@", priceRegEx)
        let result = mobileTest.evaluateWithObject(productPriceTF.text)
        if result == true {
            validationCheck[1] = true
            productPriceTF.layer.borderWidth = 0
            //            totalValidation()
        }
        else {
            validationCheck[1] = false
            productPriceTF.layer.borderWidth = 1
            productPriceTF.layer.borderColor = UIColor.redColor().CGColor
        }
    }
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{(action:UIAlertAction)->Void in  }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
}

