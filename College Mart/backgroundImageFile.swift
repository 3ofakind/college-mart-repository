//
//  backgroundImageFile.swift
//  College Mart
//
//  Created by Sankati,Vignan on 4/15/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import Foundation

extension UIView {
    func addBackground(imageName:String) {
        // screen width and height:
        let width = UIScreen.mainScreen().bounds.size.width
        let height = UIScreen.mainScreen().bounds.size.height
        
        let imageViewBackground = UIImageView(frame: CGRectMake(0, 0, width, height))
        imageViewBackground.image = UIImage(named: imageName)
        imageViewBackground.alpha = 1.0
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIViewContentMode.ScaleAspectFill
        
        self.addSubview(imageViewBackground)
        self.sendSubviewToBack(imageViewBackground)
    }}