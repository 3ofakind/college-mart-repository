//
//  FirstViewController.swift
//  College Mart
//
//  Created by Sankati,Vignan on 3/10/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var myUserProfile:UserProfile!
    
    // Products array
    var products:[ProductAd]=[]
    var productAdCollection:KCSAppdataStore!
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        myUserProfile = appDel.userProfiledel
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        /* KCSAppdataStore collection */
        productAdCollection = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ProductAdCollection",
            KCSStoreKeyCollectionTemplateClass : ProductAd.self
            ])
    }
    
    /** Number of sections here **/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    /*Title for sections*/
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Products"
    }
    
    /* Data for each row */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //AdsCell Data here
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("adsCell", forIndexPath: indexPath)
        let productName:UILabel = cell.viewWithTag(101) as! UILabel
        let productDescription:UILabel = cell.viewWithTag(102) as! UILabel
        let productPrice:UILabel = cell.viewWithTag(103) as! UILabel
        let candidateIV: UIImageView = cell.viewWithTag(104) as! UIImageView
        
        productName.text = products[indexPath.row].productName
        productDescription.text = products[indexPath.row].productDescription
        productPrice.text = "$ " + String(products[indexPath.row].price)
        
        /*Takes Image name from file entity as ID to retrieve from kinvey*/
        //image source
        let productImageName = products[indexPath.row].productImage
        if productImageName != nil && productImageName != "NULL"
        {
            
            KCSFileStore.downloadFile(
                productImageName,
                options: nil,
                completionBlock: { (downloadedResources: [AnyObject]!, error: NSError!) -> Void in
                    if error == nil {
                        let file = downloadedResources[0] as! KCSFile
                        let fileURL = file.localURL
                        
                        let image = UIImage(contentsOfFile: fileURL.path!) //note this blocks for awhile
                        candidateIV.image = image
                    } else {
                        //Add error conditions here, if any
                    }
                },
                progressBlock: nil
            )
        }
        else
        {
            /* Default Image */
            candidateIV.image = UIImage(named: "sell tag icon.png")
        }
        
        return cell
    }
    
    /* Number of rows*/
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    /* When ever this view appears */
    override func viewWillAppear(animated: Bool) {
        /** Clear all products in the array **/
        products.removeAll()
        
        let query = KCSQuery(onField: "soldStatus", withExactMatchForValue: "Available")
        productAdCollection.queryWithQuery(query,withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
            for objects in objectsOrNil
            {
                let object = objects as! ProductAd
                self.products.append(ProductAd(productName: object.productName, productDescription: object.productDescription, price: object.price, productImage: object.productImage, holdStatus: object.holdStatus, soldStatus: object.soldStatus, postedBy: object.postedBy, holdBy: object.holdBy,entityId: object.entityId!))
            }
            //Reverse the product list to show recent orders on top
            self.products = self.products.reverse()
            self.tableView.reloadData()
            },
            withProgressBlock: nil
        )
    }
    
    //Segue to show the details of the product
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let productDetails = segue.destinationViewController as! HomeProductDetailedViewController
        let indexPath = tableView.indexPathForSelectedRow?.row
        productDetails.product = products[indexPath!]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

