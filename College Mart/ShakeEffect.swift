//
//  ShakeEffect.swift
//  College Mart
//
//  Created by Sankati,Vignan on 4/18/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import Foundation

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.addAnimation(animation, forKey: "shake")
    }
}