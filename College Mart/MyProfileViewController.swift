//
//  MyProfileViewController.swift
//  College Mart
//
//  Created by Sankati,Vignan on 3/10/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController {
    
    var myUserProfile:UserProfile!
    @IBOutlet weak var nameLBL: UILabel!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var profileImageIV: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        myUserProfile = appDel.userProfiledel
        nameLBL.text = myUserProfile.firstName
        // Do any additional setup after loading the view.
        firstNameTF.text = myUserProfile.firstName
        lastNameTF.text = myUserProfile.lastName
        mobileTF.text = myUserProfile.phone
        profileImageIV.image = UIImage(named: "default_person.jpg")
        self.view.addBackground("myprofile.jpg")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "logoutSegue"
        {
            KCSUser.activeUser().logout()
        }
    }
    
    
    
}
