//
//  MYADSTableViewController.swift
//  College Mart
//
//  Created by Sankati,Vignan on 4/15/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import UIKit

class MyAdsTableViewController: UITableViewController {
    
    var myUserProfile:UserProfile!
    var products:[ProductAd]=[]
    var productAdCollection:KCSAppdataStore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Assigning the tilte
        self.title = "My Ads"
        let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        myUserProfile = appDel.userProfiledel
        self.tableView.rowHeight = 90.0
        
        // Do any additional setup after loading the view, typically from a nib.
        productAdCollection = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ProductAdCollection",
            KCSStoreKeyCollectionTemplateClass : ProductAd.self
            ])
    }
    
    override func viewWillAppear(animated: Bool) {
        products.removeAll()
        let query = KCSQuery(onField: "postedBy", withExactMatchForValue: myUserProfile.email)
        productAdCollection.queryWithQuery(query,withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
            for objects in objectsOrNil
            {
                let object = objects as! ProductAd
                self.products.append(ProductAd(productName: object.productName, productDescription: object.productDescription, price: object.price, productImage: object.productImage, holdStatus: object.holdStatus, soldStatus: object.soldStatus, postedBy: object.postedBy, holdBy: object.holdBy, entityId: object.entityId!))
            }
            //Reverse the product list to show recent orders on top
            self.products = self.products.reverse()
            self.tableView.reloadData()
            
            },
            withProgressBlock: nil
        )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return products.count
    }
    
    var deletePlanetIndexPath: NSIndexPath? = nil
    var productToDelete:ProductAd!
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            deletePlanetIndexPath = indexPath
            productToDelete = products[indexPath.row]
            confirmDelete(productToDelete)
        }
    }
    
    func confirmDelete(planet: ProductAd) {
        let alert = UIAlertController(title: "Delete Planet", message: "Are you sure you want to delete?", preferredStyle: .Alert)
        let DeleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: handleDeleteAd)
        let CancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: cancelDeleteAd)
        
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)
        
        // Support presentation in iPad
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func handleDeleteAd(alertAction: UIAlertAction!) -> Void {
        if let indexPath = deletePlanetIndexPath {
            
            // Delete collection objects from kinvey here
            tableView.beginUpdates()
            products.removeAtIndex(indexPath.row)
            // Note that indexPath is wrapped in an array:  [indexPath]
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            deletePlanetIndexPath = nil
            tableView.endUpdates()
            productAdCollection.removeObject(productToDelete, withDeletionBlock: { (deletionAdOrNil: [NSObject : AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil != nil {
                    //Include any kinvey related error message.
                } },
                withProgressBlock: nil)
        }
    }
    
    func cancelDeleteAd(alertAction: UIAlertAction!) {
        deletePlanetIndexPath = nil
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("adsCell", forIndexPath: indexPath)
        let productName:UILabel = cell.viewWithTag(101) as! UILabel
        let productDescription:UILabel = cell.viewWithTag(102) as! UILabel
        let productPrice:UILabel = cell.viewWithTag(103) as! UILabel
        let candidateIV: UIImageView = cell.viewWithTag(104) as! UIImageView
        
        
        productName.text = products[indexPath.row].productName
        
        let holdStatus = products[indexPath.row].holdStatus
        if holdStatus == "Hold"
        {
            productDescription.text =  products[indexPath.row].holdStatus + " by " + products[indexPath.row].holdBy
            productDescription.textColor = UIColor.redColor()
        }
        else
        {
            productDescription.text = "Available"
            productDescription.textColor = UIColor.greenColor()
        }
        productPrice.text = "$ " + String(products[indexPath.row].price)
        
        /*Takes Image name from file entity as ID to retrieve from kinvey*/
        //image source
        let productImageName = products[indexPath.row].productImage
        if productImageName != nil && productImageName != "NULL"
        {
            KCSFileStore.downloadFile(
                productImageName,
                options: nil,
                completionBlock: { (downloadedResources: [AnyObject]!, error: NSError!) -> Void in
                    if error == nil {
                        let file = downloadedResources[0] as! KCSFile
                        let fileURL = file.localURL
                        
                        let image = UIImage(contentsOfFile: fileURL.path!) //note this blocks for awhile
                        candidateIV.image = image
                    } else {
                        NSLog("Got an error: %@", error)
                    }
                },
                progressBlock: nil
            )
        }
        else
        {
            /* Default Image */
            candidateIV.image = UIImage(named: "sell tag icon.png")
        }
        
        return cell
    }
}
