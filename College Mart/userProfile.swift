//
//  userProfile.swift
//  College Mart
//
//  Created by Sankati,Vignan on 3/13/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import Foundation

class UserProfile :NSObject {
    var firstName:String = ""
    var lastName:String = ""
    var email:String = ""
    var phone:String!
    var entityId: String? // Kinvey entity _id -- had to add this
    
    
    override init(){
        super.init()
    }
    
    init(firstName:String, lastName:String, email:String,phone:String) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
    }
    
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "firstName" : "firstName",
            "lastName" : "lastName",
            "email" : "email",
            "phone" : "phone"
        ]
    }
    
    
    
}