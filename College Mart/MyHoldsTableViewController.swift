//
//  MyHoldsTableViewController.swift
//  College Mart
//
//  Created by Sankati,Vignan on 4/15/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import UIKit

class MyHoldsTableViewController: UITableViewController {
    
    var myUserProfile:UserProfile!
    var products:[ProductAd]=[]
    var productAdCollection:KCSAppdataStore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Assigning the tilte
        self.title = "My Holds"
        let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        myUserProfile = appDel.userProfiledel
        self.tableView.rowHeight = 90.0
        
        // Do any additional setup after loading the view, typically from a nib.
        productAdCollection = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ProductAdCollection",
            KCSStoreKeyCollectionTemplateClass : ProductAd.self
            ])
    }
    
    override func viewWillAppear(animated: Bool) {
        products.removeAll()
        let query = KCSQuery(onField: "holdBy", withExactMatchForValue: myUserProfile.email)
        /* Query the kinvey here */
        productAdCollection.queryWithQuery(query,withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
            for objects in objectsOrNil
            {
                let object = objects as! ProductAd
                self.products.append(ProductAd(productName: object.productName, productDescription: object.productDescription, price: object.price, productImage: object.productImage, holdStatus: object.holdStatus, soldStatus: object.soldStatus, postedBy: object.postedBy, holdBy: object.holdBy, entityId: object.entityId!))
            }
            //Reverse the product list to show recent orders on top
            self.products = self.products.reverse()
            self.tableView.reloadData()
            
            },
            withProgressBlock: nil
        )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return products.count
    }
    
    var deletePlanetIndexPath: NSIndexPath? = nil
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            deletePlanetIndexPath = indexPath
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("adsCell", forIndexPath: indexPath)
        let productName:UILabel = cell.viewWithTag(101) as! UILabel
        let productDescription:UILabel = cell.viewWithTag(102) as! UILabel
        let productPrice:UILabel = cell.viewWithTag(103) as! UILabel
        let candidateIV: UIImageView = cell.viewWithTag(104) as! UIImageView
        
        productName.text = products[indexPath.row].productName
        productDescription.text = products[indexPath.row].productDescription
        productPrice.text = "$ " + String(products[indexPath.row].price)
        
        /*Takes Image name from file entity as ID to retrieve from kinvey*/
        //image source
        
        let productImageName = products[indexPath.row].productImage
        if productImageName != nil && productImageName != "NULL"
        {
            KCSFileStore.downloadFile(
                productImageName,
                options: nil,
                completionBlock: { (downloadedResources: [AnyObject]!, error: NSError!) -> Void in
                    if error == nil {
                        let file = downloadedResources[0] as! KCSFile
                        let fileURL = file.localURL
                        
                        let image = UIImage(contentsOfFile: fileURL.path!) //note this blocks for awhile
                        candidateIV.image = image
                    } else {
                        NSLog("Got an error: %@", error)
                    }
                },
                progressBlock: nil
            )
        }
        else
        {
            /* Default Image */
            candidateIV.image = UIImage(named: "sell tag icon.png")
        }
        
        return cell
    }
    
}
