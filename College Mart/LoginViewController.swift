//
//  LoginViewController.swift
//  College Mart
//
//  Created by Sankati,Vignan on 3/13/16.
//  Copyright © 2016 Sankati,Vignan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var loginBTN: UIButton!
    
    var appDel:AppDelegate!
    var userProfile:UserProfile!
    var userprofileCollection:KCSAppdataStore!
    var validationCheck:[Bool] = [false,false]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTF.delegate = self
        passwordTF.delegate = self
        
        /* AppDelegate */
        appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        self.userProfile = appDel.userProfiledel
        
        /* KCSAppdataStore collection */
        userprofileCollection = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "userProfile",
            KCSStoreKeyCollectionTemplateClass : UserProfile.self
            ])
        
        self.view.addBackground("login1.jpg")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let nextTag: NSInteger = textField.tag + 1;
        // Try to find next responder
        if let nextResponder: UIResponder! = textField.superview!.viewWithTag(nextTag){
            nextResponder.becomeFirstResponder()
        }
        else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        return false // We do not want UITextField to insert line-breaks.
    }
    
    //Validation for email textbox
    @IBAction func emailIdEditEnd(sender: AnyObject) {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluateWithObject(usernameTF.text)
        if result == true {
            usernameTF.layer.borderWidth = 0
            validationCheck[0] = true
            totalValidation()
        }
        else {
            usernameTF.shake()
            validationCheck[0] = false
            usernameTF.layer.borderWidth = 1
            usernameTF.layer.borderColor = UIColor.redColor().CGColor
        }
    }
    
    //Validation for password textbox
    @IBAction func passwordEditEnd(sender: AnyObject) {
        if passwordTF.text != "" {
            passwordTF.layer.borderWidth = 0
            validationCheck[1] = true
            totalValidation()
        }
        else {
            passwordTF.shake()
            validationCheck[1] = true
            passwordTF.layer.borderWidth = 1
            passwordTF.layer.borderColor = UIColor.redColor().CGColor
        }
    }
    
    //Entire validations
    func totalValidation() -> Bool {
        if validationCheck[0] && validationCheck[1] {
            return true
        }
        else {
            return false
        }
    }
    
    //Alert box
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{(action:UIAlertAction)->Void in  }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    
    //Function to perform when login button is clicked
    func loginBTNAction() -> Bool {
        var checkForUserValidation:Bool = false
        KCSUser.loginWithUsername(
            usernameTF.text,
            password: passwordTF.text,
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil {
                    if self.totalValidation() {
                        self.performSegueWithIdentifier("login_segue", sender: nil)
                        let query = KCSQuery(onField: "email", withExactMatchForValue: "\(self.usernameTF.text!)")
                        self.userprofileCollection.queryWithQuery(query,withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                            for objs in objectsOrNil
                            {
                                let obj = objs as! UserProfile
                                self.userProfile = UserProfile(firstName: obj.firstName, lastName: obj.lastName, email: obj.email, phone: obj.phone)
                                self.appDel.userProfiledel = self.userProfile
                            }
                            
                            },
                            
                            withProgressBlock: nil
                            
                        )
                        checkForUserValidation = true
                        //hide log-in view and show main app content
                    } else {
                        self.displayAlertControllerWithTitle("Login failed", message: "Please provide valid email/password")
                    }
                    
                } else {
                    //there was an error with the update save
                    self.displayAlertControllerWithTitle("Login failed", message: "Please provide valid email/password")
                    checkForUserValidation = false
                }
            }
        )
        return checkForUserValidation
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        return loginBTNAction()
    }
}
